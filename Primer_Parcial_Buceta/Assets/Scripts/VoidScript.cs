using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class VoidScript : MonoBehaviour
{
    public GameObject Player;
    public UnityEvent triggeredEvent;
    Rigidbody rb;
    BoxCollider col;
    CapsuleCollider colPlayer;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<BoxCollider>();
        colPlayer = Player.GetComponent<CapsuleCollider>();

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == Player)
        {
            triggeredEvent.Invoke();
        }
    }

    void Respawnear()
    {
        this.transform.position = new Vector3(-0.1f, 2f, 3.15f);
        this.transform.rotation = Quaternion.Euler(0f, -90f, 0f);
        
    }
   
}
