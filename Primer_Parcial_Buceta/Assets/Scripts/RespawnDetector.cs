using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnDetector : MonoBehaviour
{
    public GameObject playerGO;

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("Player"))
        {
            playerGO.transform.position = new Vector3(-0.1f, 2f, 3.15f);
            playerGO.transform.rotation = Quaternion.Euler(0f, -90f, 0f);
        }
            
    }


}
