using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrownScript : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("Player"))
        {
            gameObject.transform.position = new Vector3(-0.521f, 0.597f, -8.745f);
            gameObject.SetActive(false);
        }

    }
}
