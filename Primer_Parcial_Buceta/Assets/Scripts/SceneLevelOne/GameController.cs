using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameController : MonoBehaviour
{
    public GameObject playerGO;  
    float timeLeft;
    public TMP_Text timerText;

    void Start()
    {
        ComenzarJuego();
    }

    void Update()
    {
        if (timeLeft == 0)
        {
            SceneManager.LoadScene(1);
        }
    }

    void ComenzarJuego()
    {
        playerGO.transform.position = new Vector3(0, 0, 0);
        playerGO.transform.rotation = Quaternion.Euler(0f, 0, 0f);

        StartCoroutine(ComenzarCronometro(60));
    }

    public IEnumerator ComenzarCronometro(float valorCronometro = 60)
    {
        timeLeft = valorCronometro;
        while (timeLeft > 0)
        {
            Debug.Log("Restan " + timeLeft + " segundos.");
            yield return new WaitForSeconds(1.0f);
            timerText.text = "" + timeLeft.ToString("f0");
            timeLeft--;
        }
    }

}
