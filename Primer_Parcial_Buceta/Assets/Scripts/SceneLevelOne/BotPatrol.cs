using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotPatrol : MonoBehaviour
{
    bool tengoQueBajar = false;
    int rapidez = 3;

    void Update()
    {
        if (transform.position.x >= 4.75f)
        {
            tengoQueBajar = true;
        }
        if (transform.position.x <= -4)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }

    }

    void Subir()
    {
        transform.position += transform.right * rapidez * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.right * rapidez * Time.deltaTime;
    }

}
