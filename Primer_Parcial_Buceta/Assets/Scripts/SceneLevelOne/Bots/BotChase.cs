using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotChase : MonoBehaviour
{
   // private int hp;
    private GameObject playerGO;
    public int speed;


    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("Player"))
        {
            playerGO.transform.position = new Vector3(-0.1f, 2f, 3.15f);
            playerGO.transform.rotation = Quaternion.Euler(0f, -90f, 0f);
        }

    }

    void Start()
    {
       // hp = 100;
        //Otra forma de obtener un GameObject sin arrastrarlo desde el editor
        playerGO = GameObject.Find("Player");
    }

    private void Update()
    {
        transform.LookAt(playerGO.transform);
        transform.Translate(speed * Vector3.forward * Time.deltaTime);
    }

 

    



}
