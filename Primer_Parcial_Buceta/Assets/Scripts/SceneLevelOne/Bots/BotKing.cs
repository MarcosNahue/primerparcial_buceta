using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BotKing : MonoBehaviour
{
    public GameObject playerGO;
    public NavMeshAgent agent;
    public float DistanceRun = 4f;

    void Start()
    {
        playerGO = GameObject.Find("Player");

    }

    void Update()
    {
        float distance = Vector3.Distance(transform.position, playerGO.transform.position);

        if (distance < DistanceRun)
        {
            Vector3 directionToPlayer = transform.position - playerGO.transform.position;

            Vector3 newPos = transform.position + directionToPlayer;

            agent.SetDestination(newPos);
        
        }
        
    }

}
