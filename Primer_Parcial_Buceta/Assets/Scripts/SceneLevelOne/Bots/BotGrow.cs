using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotGrow : MonoBehaviour
{
    bool tengoQueBajar = false;
    int rapidez = 3;

    public GameObject playerGO;

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("Player"))
        {
            playerGO.transform.position = new Vector3(-0.1f, 2f, 3.15f);
            playerGO.transform.rotation = Quaternion.Euler(0f, -90f, 0f);
        }

    }

    void Update()
    {
        if (transform.localScale.y >= 5f)
        {
            tengoQueBajar = true;
        }
        if (transform.localScale.y <= 0.08531354f)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }

    }

    void Subir()
    {
        transform.localScale += transform.up * rapidez * Time.deltaTime;
    }

    void Bajar()
    {
        transform.localScale -= transform.up * rapidez * Time.deltaTime;
    }

}
