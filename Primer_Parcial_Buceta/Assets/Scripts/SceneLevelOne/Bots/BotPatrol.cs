using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotPatrol : MonoBehaviour
{
    bool tengoQueBajar = false;
    int rapidez = 3;

    public GameObject playerGO;

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("Player"))
        {
            playerGO.transform.position = new Vector3(-0.1f, 2f, 3.15f);
            playerGO.transform.rotation = Quaternion.Euler(0f, -90f, 0f);
        }

    }

    void Start()
    {
        playerGO = GameObject.Find("Player");
    }

    void Update()
    {
        if (transform.position.x >= 4.75f)
        {
            tengoQueBajar = true;
        }
        if (transform.position.x <= -4)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }

    }

    void Subir()
    {
        transform.position += transform.right * rapidez * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.right * rapidez * Time.deltaTime;
    }

}
