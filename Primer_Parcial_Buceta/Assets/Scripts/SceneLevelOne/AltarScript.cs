using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AltarScript : MonoBehaviour
{
    public GameObject crownGO;
    BoxCollider colliderGO;

    void Start()
    {
        colliderGO = GetComponent<BoxCollider>();
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("Player") && crownGO.activeSelf == false)
        {
            crownGO.SetActive(true);
            SceneManager.LoadScene(2);

            //colliderGO.enabled = false;
        }

    }

  
}
