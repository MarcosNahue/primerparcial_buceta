using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotChase : MonoBehaviour
{
   // private int hp;
    private GameObject playerGO;
    public int speed;

    void Start()
    {
       // hp = 100;
        //Otra forma de obtener un GameObject sin arrastrarlo desde el editor
        playerGO = GameObject.Find("Player");
    }

    private void Update()
    {
        transform.LookAt(playerGO.transform);
        transform.Translate(speed * Vector3.forward * Time.deltaTime);
    }

 

    



}
