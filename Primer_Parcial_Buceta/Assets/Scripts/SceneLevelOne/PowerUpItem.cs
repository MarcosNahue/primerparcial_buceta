using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpItem : MonoBehaviour
{
    public GameObject playerGO;
    CapsuleCollider col;
    PlayerController scriptPlayer;
    GameObject thisGO;

    void Start()
    {
        scriptPlayer = FindObjectOfType<PlayerController>();
        col = playerGO.GetComponent<CapsuleCollider>();
    }

    private void OnTriggerEnter(Collider other)
    {
       
        if (other.CompareTag("Player"))
        {
            playerGO.transform.localScale = new Vector3(1f, 1.6f, 1f);
            
            col.height = 2;
            col.radius = 0.5f;
            col.center = new Vector3(0, 0, 0);
            col.direction = 1;
            scriptPlayer.rapidezDesplazamiento = 7f;
            this.gameObject.SetActive(false);
        }
    }
}
