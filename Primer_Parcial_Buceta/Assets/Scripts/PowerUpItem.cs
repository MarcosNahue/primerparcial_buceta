using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpItem : MonoBehaviour
{
    public GameObject playerGO;
    CapsuleCollider col;
    PlayerController scriptPlayer;
    GameObject thisGO;

    void Start()
    {
        scriptPlayer = FindObjectOfType<PlayerController>();
        col = playerGO.GetComponent<CapsuleCollider>();
    }

    private void OnTriggerEnter(Collider other)
    {
       
        if (other.CompareTag("Player"))
        {
            playerGO.transform.localScale = new Vector3(0f, 1f, 0f);
            col.height = 2;
            scriptPlayer.rapidezDesplazamiento = 7f;
            this.gameObject.SetActive(false);
        }
    }
}
