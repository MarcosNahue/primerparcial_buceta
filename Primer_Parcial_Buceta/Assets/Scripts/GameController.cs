using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public GameObject jugador;
    /*public GameObject bot;
    private List<GameObject> listaEnemigos = new List<GameObject>();*/
    float tiempoRestante;

    void Start()
    {
        ComenzarJuego();
    }

    void Update()
    {
        if (tiempoRestante == 0)
        {
            SceneManager.LoadScene(1);
        }
    }

    void ComenzarJuego()
    {
        jugador.transform.position = new Vector3(-0.1f, 2f, 3.15f);
        jugador.transform.rotation = Quaternion.Euler(0f, -90f, 0f);

        /*foreach (GameObject item in listaEnemigos)
        {
            Destroy(item);
        }

        listaEnemigos.Add(Instantiate(bot, new Vector3(5, 1f, 3f), Quaternion.identity));
        listaEnemigos.Add(Instantiate(bot, new Vector3(3, 1f, 3f), Quaternion.identity));
        listaEnemigos.Add(Instantiate(bot, new Vector3(1, 1f, 3f), Quaternion.identity));*/

        StartCoroutine(ComenzarCronometro(60));
    }

    public IEnumerator ComenzarCronometro(float valorCronometro = 60)
    {
        tiempoRestante = valorCronometro;
        while (tiempoRestante > 0)
        {
            Debug.Log("Restan " + tiempoRestante + " segundos.");
            yield return new WaitForSeconds(1.0f);
            tiempoRestante--;
        }
    }

}
