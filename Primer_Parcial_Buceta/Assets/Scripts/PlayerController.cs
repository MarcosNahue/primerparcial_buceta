using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float rapidezDesplazamiento = 5.0f;
    public LayerMask capaPiso;
    public float magnitudSalto;
    Rigidbody rb;
    CapsuleCollider col;
    bool yaSalto = false;

    void Start()
    {

        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();


        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        Salto();
        Respawnear();
    }
    void Respawnear() 
    {
        if(Input.GetKeyDown(KeyCode.R))
        {
            this.transform.position = new Vector3(-0.1f, 2f, 3.15f);
            this.transform.rotation = Quaternion.Euler(0f, -90f, 0f);
        }
        
    }

    void Salto() 
    {
        if (Input.GetKeyDown(KeyCode.Space) && EstaEnPiso()) //Primer salto
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            yaSalto = false;
        }

        if (Input.GetKeyDown(KeyCode.Space) && yaSalto == false && EstaEnPiso() == false) //Doble salto
        {
            rb.AddForce(Vector3.up * (magnitudSalto + 1), ForceMode.Impulse);
            yaSalto = true;
        }

    }

    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.min, new Vector3(col.bounds.center.x,col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
       
    }

}
